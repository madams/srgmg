#PBS -q regular
##PBS -l mppwidth=24
#PBS -l mppwidth=98304
##PBS -l mppwidth=1536
##PBS -l mppwidth=12288
#PBS -l walltime=0:30:00
#PBS -N srgmg
#PBS -V 
#PBS -j eo
#PBS -A m499

cd $PBS_O_WORKDIR

# -S 4 for Hopper, -S 8 for Edison

export ARGS='-verbose 2 -show_options true -nsmoothsfmg 1 -nsmoothsdown 2 -nsmoothsup 2 -out_error true -dom_x_hi 2.0'
#
# N=32, redudnant coarse grid solves
#
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -num_solves 512 -nxpe 64 -nype 32 -nzpe 32 >& out_Fcycle_065536_nx032_red
/bin/mv Convergence.history Convergence.Fcycle.065536cores.032N.red.history
/bin/mv Run.history Run.Fcycle.065536cores.032N.red.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -num_solves 512 -nxpe 32 -nype 16 -nzpe 16 >& out_Fcycle_008192_nx032_red
/bin/mv Convergence.history Convergence.Fcycle.008192cores.032N.red.history
/bin/mv Run.history Run.Fcycle.008192cores.032N.red.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -num_solves 512 -nxpe 16 -nype 8 -nzpe 8 >& out_Fcycle_001024_nx032_red
/bin/mv Convergence.history Convergence.Fcycle.001024cores.032N.red.history
/bin/mv Run.history Run.Fcycle.001024cores.032N.red.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -num_solves 512 -nxpe 8 -nype 4 -nzpe 4 >& out_Fcycle_000128_nx032_red
/bin/mv Convergence.history Convergence.Fcycle.000128cores.032N.red.history
/bin/mv Run.history Run.Fcycle.000128cores.032N.red.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -num_solves 512 -nxpe 4 -nype 2 -nzpe 2 >& out_Fcycle_000016_nx032_red
/bin/mv Convergence.history Convergence.Fcycle.000016cores.032N.red.history
/bin/mv Run.history Run.Fcycle.000016cores.032N.red.history
#
export ARGS='-verbose 2 -show_options true -nsmoothsfmg 1 -nsmoothsdown 2 -nsmoothsup 2 -out_error true -dom_x_hi 2.0 -redundant_crs false'
#
# N=128
#
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nxpe 64 -nype 32 -nzpe 32 -num_solves 8 >& out_Fcycle_065536_nx128_red
/bin/mv Convergence.history Convergence.Fcycle.065536cores.128N.red.history
/bin/mv Run.history Run.Fcycle.065536cores.128N.red.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nxpe 32 -nype 16 -nzpe 16 -num_solves 8 >& out_Fcycle_008192_nx128_red
/bin/mv Convergence.history Convergence.Fcycle.008192cores.128N.red.history
/bin/mv Run.history Run.Fcycle.008192cores.128N.red.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nxpe 16 -nype 8 -nzpe 8 -num_solves 8 >& out_Fcycle_001024_nx128_red
/bin/mv Convergence.history Convergence.Fcycle.001024cores.128N.red.history
/bin/mv Run.history Run.Fcycle.001024cores.128N.red.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nxpe 8 -nype 4 -nzpe 4 -num_solves 8 >& out_Fcycle_000128_nx128_red
/bin/mv Convergence.history Convergence.Fcycle.000128cores.128N.red.history
/bin/mv Run.history Run.Fcycle.000128cores.128N.red.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128  -nxpe 4 -nype 2 -nzpe 2 -num_solves 8 >& out_Fcycle_000016_nx128_red
/bin/mv Convergence.history Convergence.Fcycle.000016cores.128N.red.history
/bin/mv Run.history Run.Fcycle.000016cores.128N.red.history
#
# N=128, SR
#
export SR='-sr_max_loc_sz 128 -nxloc 8 '
#
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 64 -nype 32 -nzpe 32 ${SR} -num_solves 8 >& out_Fcycle_065536_nx128_SR
/bin/mv Convergence.history Convergence.Fcycle.065536cores.128N.SR.history
/bin/mv Run.history Run.Fcycle.065536cores.128N.SR.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 32 -nype 16 -nzpe 16 ${SR} -num_solves 8 >& out_Fcycle_008192_nx128_SR
/bin/mv Convergence.history Convergence.Fcycle.008192cores.128N.SR.history
/bin/mv Run.history Run.Fcycle.008192cores.128N.SR.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 16 -nype 8 -nzpe 8 ${SR} -num_solves 8 >& out_Fcycle_001024_nx128_SR
/bin/mv Convergence.history Convergence.Fcycle.001024cores.128N.SR.history
/bin/mv Run.history Run.Fcycle.001024cores.128N.SR.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 8 -nype 4 -nzpe 4 ${SR} -num_solves 8 >& out_Fcycle_000128_nx128_SR
/bin/mv Convergence.history Convergence.Fcycle.000128cores.128N.SR.history
/bin/mv Run.history Run.Fcycle.000128cores.128N.SR.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS}  -nxpe 4 -nype 2 -nzpe 2 ${SR} -num_solves 8 >& out_Fcycle_000016_nx128_SR
/bin/mv Convergence.history Convergence.Fcycle.000016cores.128N.SR.history
/bin/mv Run.history Run.Fcycle.000016cores.128N.SR.history
#
# N=32, SR, non redudant coarse grid sovles
#
export SR='-sr_max_loc_sz 32 -nxloc 4 -num_solves 512 '
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 64 -nype 32 -nzpe 32 ${SR}  >& out_Fcycle_065536_nx032_SR
/bin/mv Convergence.history Convergence.Fcycle.065536cores.032N.SR.history
/bin/mv Run.history Run.Fcycle.065536cores.032N.SR.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 32 -nype 16 -nzpe 16  ${SR} >& out_Fcycle_008192_nx032_SR
/bin/mv Convergence.history Convergence.Fcycle.008192cores.032N.SR.history
/bin/mv Run.history Run.Fcycle.008192cores.032N.SR.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 16 -nype 8 -nzpe 8  ${SR} >& out_Fcycle_001024_nx032_SR
/bin/mv Convergence.history Convergence.Fcycle.001024cores.032N.SR.history
/bin/mv Run.history Run.Fcycle.001024cores.032N.SR.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 8 -nype 4 -nzpe 4  ${SR} >& out_Fcycle_000128_nx032_SR
/bin/mv Convergence.history Convergence.Fcycle.000128cores.032N.SR.history
/bin/mv Run.history Run.Fcycle.000128cores.032N.SR.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxpe 4 -nype 2 -nzpe 2  ${SR} >& out_Fcycle_000016_nx032_SR
/bin/mv Convergence.history Convergence.Fcycle.000016cores.032N.SR.history
/bin/mv Run.history Run.Fcycle.000016cores.032N.SR.history
#
# N=32, non-redundent coarse grid solve
#
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -nxpe 64 -nype 32 -nzpe 32 -num_solves 512  >& out_Fcycle_065536_nx032_nonRCG
/bin/mv Convergence.history Convergence.Fcycle.065536cores.032N.nonRCG.history
/bin/mv Run.history Run.Fcycle.065536cores.032N.nonRCG.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -nxpe 32 -nype 16 -nzpe 16 -num_solves 512  >& out_Fcycle_008192_nx032_nonRCG
/bin/mv Convergence.history Convergence.Fcycle.008192cores.032N.nonRCG.history
/bin/mv Run.history Run.Fcycle.008192cores.032N.nonRCG.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -nxpe 16 -nype 8 -nzpe 8 -num_solves 512  >& out_Fcycle_001024_nx032_nonRCG
/bin/mv Convergence.history Convergence.Fcycle.001024cores.032N.nonRCG.history
/bin/mv Run.history Run.Fcycle.001024cores.032N.nonRCG.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32 -nxpe 8 -nype 4 -nzpe 4 -num_solves 512  >& out_Fcycle_000128_nx032_nonRCG
/bin/mv Convergence.history Convergence.Fcycle.000128cores.032N.nonRCG.history
/bin/mv Run.history Run.Fcycle.000128cores.032N.nonRCG.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 32  -nxpe 4 -nype 2 -nzpe 2 -num_solves 512  >& out_Fcycle_000016_nx032_nonRCG
/bin/mv Convergence.history Convergence.Fcycle.000016cores.032N.nonRCG.history
/bin/mv Run.history Run.Fcycle.000016cores.032N.nonRCG.history
#
# V-cycles, N=128, non-redundent coarse grid solve
#
aprun -n 65536 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nvcycles 20 -nfcycles 0 -rtol 1.e-4 -nxpe 64 -nype 32 -nzpe 32 -num_solves 8 >& out_Vcycles_065536_nx128
/bin/mv Convergence.history Convergence.Vcycles.065536cores.128N.history
/bin/mv Run.history Run.Vcycles.065536cores.128N.history

aprun -n 8192 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nvcycles 100 -nfcycles 0 -rtol 1.e-4 -nxpe 32 -nype 16 -nzpe 16 -num_solves 8 >& out_Vcycles_008192_nx128
/bin/mv Convergence.history Convergence.Vcycles.008192cores.128N.history
/bin/mv Run.history Run.Vcycles.008192cores.128N.history

aprun -n 1024 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nvcycles 100 -nfcycles 0 -rtol 1.e-4 -nxpe 16 -nype 8 -nzpe 8 -num_solves 8 >& out_Vcycles_001024_nx128
/bin/mv Convergence.history Convergence.Vcycles.001024cores.128N.history
/bin/mv Run.history Run.Vcycles.001024cores.128N.history

aprun -n 128 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nvcycles 100 -nfcycles 0 -rtol 1.e-4 -nxpe 8 -nype 4 -nzpe 4 -num_solves 8 >& out_Vcycles_000128_nx128
/bin/mv Convergence.history Convergence.Vcycles.000128cores.128N.history
/bin/mv Run.history Run.Vcycles.000128cores.128N.history

aprun -n 16 -S 8 ../../src/pms.${PETSC_ARCH}.ex ${ARGS} -nxloc 128 -nvcycles 100 -nfcycles 0 -rtol 1.e-4 -nxpe 4 -nype 2 -nzpe 2 -num_solves 8 >& out_Vcycles_000016_nx128
/bin/mv Convergence.history Convergence.Vcycles.000016cores.128N.history
/bin/mv Run.history Run.Vcycles.000016cores.128N.history
