close all
%    K B0
plot(5,3,'b*'), hold on
plot(3,1,'ro')
plot(3,2,'ro')
plot(4,3,'ro')
plot(5,4,'ro')
plot(6,5,'ro')
plot(6,9,'b*')
plot(5,6,'b*')
plot(4,3,'b*')
plot(6,10,'b*')
plot(5,5,'b*')
plot(4,3,'b*')
plot(6,9,'b*')
plot(5,4,'b*')
plot(4,4,'b*')
V=axis;
V(1)=2.5;
V(2)=6.5;
V(3)=0;
V(4)=11;
axis(V);
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',24,'fontWeight','bold')
%set(gca,'XTickLabel',['3  ';'4  ';'5  ';'6  '])
ylabel('NB_0');
xlabel('K with error ratio < 1.1')
legend('NB = A + B*(K-i)','Maximum SR buffer schedual',2)
title(['K vs, NB_0 vs. K with error ration < 1.1'])
print(gcf,'-djpeg100','B0_K')
print(gcf,'-depsc2','B0_K')
