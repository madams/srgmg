!-----------------------------------------------------------------
!       Ravi Samtaney & Mark Adams
!       Copyright 2014
!-----------------------------------------------------------------
subroutine exchange(ux,p,t)
  use discretization,only:nvar,nsg
  use base_data_module
  use mpistuff
  use tags
  use globals,only:mg_ref_ratio,redundant_crs
  implicit none
  type(topot),intent(in)::t
  type(patcht),intent(in)::p
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)

  call exchange_new(ux,p,t)

  return 
end subroutine exchange
! normal exchange
subroutine exchange_old(ux,p,t)
  use discretization,only:nvar,nsg
  use base_data_module
  use mpistuff
  use tags
  use globals,only:mg_ref_ratio,redundant_crs
  implicit none
  type(topot),intent(in)::t
  type(patcht),intent(in)::p
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)

  if (.not.redundant_crs.and.t%redundant) then
     if (t%comm3d/=mpi_comm_null) then ! active will inc this - yuck
        MSG_XCH_XLOW_TAG=MSG_XCH_XLOW_TAG + 1
        MSG_XCH_XHI_TAG=MSG_XCH_XHI_TAG + 1
        MSG_XCH_YLOW_TAG=MSG_XCH_YLOW_TAG + 1
        MSG_XCH_YHI_TAG=MSG_XCH_YHI_TAG + 1
        MSG_XCH_ZLOW_TAG=MSG_XCH_ZLOW_TAG + 1
        MSG_XCH_ZHI_TAG=MSG_XCH_ZHI_TAG + 1
     end if
     return
  end if

#ifdef HAVE_PETSC
  call PetscLogEventBegin(events(11),ierr)
#endif
  if (t%comm3d==mpi_comm_null) then
     ! noop
  else
     call XExchange(ux,p,t,nsg%i(1))
     call YExchange(ux,p,t,nsg%i(2))
#ifndef TWO_D
     call ZExchange(ux,p,t,nsg%i(3))
#endif
  end if
#ifdef HAVE_PETSC
  call PetscLogEventEnd(events(11),ierr)
#endif
  return
end subroutine Exchange_old
! set BCs
subroutine setBCs(ux,p,t)
  use discretization,only:nvar
  use base_data_module
  use mpistuff
  implicit none
  type(topot),intent(in)::t
  type(patcht),intent(in)::p
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  !
#ifdef HAVE_PETSC
  call PetscLogEventBegin(events(6),ierr)
#endif
  call doXBC(ux,p,t,-1.d0)
  call doYBC(ux,p,t,-1.d0)
#ifndef TWO_D
  call doZBC(ux,p,t,-1.d0)
#endif
#ifdef HAVE_PETSC
  call PetscLogEventEnd(events(6),ierr)
#endif
  return
end subroutine setBCs
! set BCs
subroutine zeroBCs(ux,p,t)
  use discretization,only:nvar
  use base_data_module
  use mpistuff
  implicit none
  type(topot),intent(in)::t
  type(patcht),intent(in)::p
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  !
  stop 'zeroBCs not used???'
#ifdef HAVE_PETSC
  call PetscLogEventBegin(events(6),ierr)
#endif
  call doXBC(ux,p,t,0.d0)
  call doYBC(ux,p,t,0.d0)
#ifndef TWO_D
  call doZBC(ux,p,t,0.d0)
#endif
#ifdef HAVE_PETSC
  call PetscLogEventEnd(events(6),ierr)
#endif
  return
end subroutine zeroBCs
!-----------------------------------------------------------------------
subroutine XExchange(ux,p,t,ng)
  use discretization, only:nvar
  use base_data_module
  use mpistuff
  use tags
  implicit none
  integer,intent(in)::ng
  type(patcht),intent(in)::p
  type(topot),intent(in)::t
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  ! allocating on stack
  double precision:: xbuffer_send(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  double precision:: xbuffer_recv(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  integer:: XBUFFSIZE
  integer:: jj,mm,kk
  integer:: msg_id_send_x_low
  integer:: msg_id_send_x_hi
  integer:: msg_id_recv_x_low
  integer:: msg_id_recv_x_hi  
  !       -------X DIRECTION COMMUNICATION
  !       Update x-low boundaries
  XBUFFSIZE = size(xbuffer_send)

#ifndef XPERIODIC
  if (t%ipe%i(1).gt.1) then
#endif
     call MPI_Irecv(xbuffer_recv, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%left,MSG_XCH_XLOW_TAG,t%comm3D,msg_id_recv_x_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef XPERIODIC
  endif
#endif

#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              xbuffer_send(mm,jj,kk,:) = ux(p%max%hi%i(1)+1-mm,jj,kk,:)
           enddo
        enddo
     enddo
     if (t%right<0)stop 'p%t%right<0'
     call MPI_Isend(xbuffer_send, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%right,MSG_XCH_XLOW_TAG,t%comm3D,msg_id_send_x_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef XPERIODIC
  endif
#endif
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     call MPI_Wait(msg_id_recv_x_low,status,ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              ux(1-mm,jj,kk,:) = xbuffer_recv(mm,jj,kk,:)
           enddo
        enddo
     enddo
#ifndef XPERIODIC
  endif
#endif 
 
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Wait(msg_id_send_x_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef XPERIODIC
  endif
#endif  
  !	update x-high boundaries
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Irecv(xbuffer_recv, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%right, MSG_XCH_XHI_TAG, t%comm3D,msg_id_recv_x_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef XPERIODIC
  endif
#endif
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              xbuffer_send(mm,jj,kk,:) = ux(mm,jj,kk,:)
           enddo
        enddo
     enddo

     if (t%left<0)stop 't%left<0'
     call MPI_Isend(xbuffer_send, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%left, MSG_XCH_XHI_TAG, t%comm3D, msg_id_send_x_hi,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef XPERIODIC
  endif
#endif
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Wait(msg_id_recv_x_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              ux(p%max%hi%i(1)+mm,jj,kk,:) = xbuffer_recv(mm,jj,kk,:)
           enddo
        enddo
     enddo
#ifndef XPERIODIC
  endif
#endif
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     call MPI_Wait(msg_id_send_x_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef XPERIODIC
  endif
#endif
  ! keep from getting mixed up 
  MSG_XCH_XLOW_TAG = MSG_XCH_XLOW_TAG+1
  MSG_XCH_XHI_TAG = MSG_XCH_XHI_TAG+1
  return
end subroutine XExchange
!-----------------------------------------------------------------------
subroutine YExchange(ux,p,t,ng)
  use discretization, only:nvar
  use base_data_module
  use mpistuff
  use tags
  implicit none
  integer,intent(in)::ng
  type(patcht),intent(in) :: p
  type(topot),intent(in)::t
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)

  double precision:: ybuffer_send(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  double precision:: ybuffer_recv(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  integer:: YBUFFSIZE
  integer:: ii,mm,kk
  integer:: msg_id_send_y_low
  integer:: msg_id_send_y_hi
  integer:: msg_id_recv_y_low
  integer:: msg_id_recv_y_hi
  ! -------Y DIRECTION COMMUNICATION
  !	update y-low boundaries
  YBUFFSIZE=size(ybuffer_recv)

#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call  MPI_Irecv(ybuffer_recv, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%bottom,MSG_XCH_YLOW_TAG, t%comm3D,msg_id_recv_y_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef YPERIODIC
  endif
#endif  

#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif  
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do kk=1,p%max%hi%i(3)
           do mm=1,ng
              ybuffer_send(ii,mm,kk,:) = ux(ii,p%max%hi%i(2)+1-mm,kk,:)
           enddo
        enddo
     enddo
     if (t%top<0)stop 't%top<0'
     call MPI_Isend(ybuffer_send, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%top,MSG_XCH_YLOW_TAG,t%comm3D,msg_id_send_y_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef YPERIODIC
  endif
#endif

#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_recv_y_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)     
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ux(ii,1-mm,kk,:) = ybuffer_recv(ii,mm,kk,:)
           enddo
        enddo
     enddo
#ifndef YPERIODIC
  endif
#endif  
  
#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif
     call MPI_Wait(msg_id_send_y_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef YPERIODIC
  endif
#endif
  !	update y-high boundaries
#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif  
     call MPI_Irecv(ybuffer_recv, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%top, MSG_XCH_YHI_TAG, t%comm3D, msg_id_recv_y_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef YPERIODIC
  endif
#endif  
  
#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ybuffer_send(ii,mm,kk,:) = ux(ii,mm,kk,:)
           enddo
        enddo
     enddo
     if (t%bottom<0)stop 't%bottom<0'
     call MPI_Isend(ybuffer_send, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%bottom, MSG_XCH_YHI_TAG, t%comm3D, msg_id_send_y_hi,ierr)  
     call errorhandler(ierr,ERROR_SEND)
#ifndef YPERIODIC
  endif
#endif

#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif
     call MPI_Wait(msg_id_recv_y_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ux(ii,p%max%hi%i(2)+mm,kk,:) = ybuffer_recv(ii,mm,kk,:)
           enddo
        enddo
     enddo
#ifndef YPERIODIC
  endif
#endif    
#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_send_y_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef YPERIODIC
  endif
#endif  
  ! keep from getting mixed up 
  MSG_XCH_YLOW_TAG = MSG_XCH_YLOW_TAG+1
  MSG_XCH_YHI_TAG = MSG_XCH_YHI_TAG+1
  return
end subroutine YExchange
!-----------------------------------------------------------------------
subroutine ZExchange(ux,p,t,ng)
  use discretization, only:nvar
  use base_data_module
  use mpistuff
  use tags
  implicit none
  integer,intent(in)::ng
  type(patcht),intent(in) :: p
  type(topot),intent(in)::t
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  ! locals on stack
  double precision:: zbuffer_send(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  double precision:: zbuffer_recv(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  integer:: ZBUFFSIZE
  integer:: ii,jj,mm
  integer:: msg_id_send_z_low
  integer:: msg_id_send_z_hi
  integer:: msg_id_recv_z_low
  integer:: msg_id_recv_z_hi
  ! -------Z DIRECTION COMMUNICATION
  !	update z-low boundaries
  ZBUFFSIZE=size(zbuffer_recv)

#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Irecv(zbuffer_recv, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%behind,MSG_XCH_ZLOW_TAG, t%comm3D,msg_id_recv_z_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef ZPERIODIC
  endif
#endif  
  
#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif  
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              zbuffer_send(ii,jj,mm,:) = ux(ii,jj,p%max%hi%i(3)+1-mm,:)
           enddo
        enddo
     enddo
     if (t%forward<0)stop 't%forward<0'
     call MPI_Isend(zbuffer_send, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%forward,MSG_XCH_ZLOW_TAG,t%comm3D,msg_id_send_z_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef ZPERIODIC
  endif
#endif

#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_recv_z_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              ux(ii,jj,1-mm,:) = zbuffer_recv(ii,jj,mm,:)
           enddo
        enddo
     enddo
#ifndef ZPERIODIC
  endif
#endif  
  
#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif
     call MPI_Wait(msg_id_send_z_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef ZPERIODIC
  endif
#endif
  !	update z-high boundaries
#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif  
     call MPI_Irecv(zbuffer_recv, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%forward, MSG_XCH_ZHI_TAG, t%comm3D, msg_id_recv_z_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef ZPERIODIC
  endif
#endif  
  
#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              zbuffer_send(ii,jj,mm,:) = ux(ii,jj,mm,:)
           enddo
        enddo
     enddo
     if (t%behind<0)stop 't%behind<0'
     call MPI_Isend(zbuffer_send, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%behind, MSG_XCH_ZHI_TAG, t%comm3D, msg_id_send_z_hi,ierr)  
     call errorhandler(ierr,ERROR_SEND)
#ifndef ZPERIODIC
  endif
#endif

#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif
     call MPI_Wait(msg_id_recv_z_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              ux(ii,jj,p%max%hi%i(3)+mm,:) = zbuffer_recv(ii,jj,mm,:)
           enddo
        enddo
     enddo
#ifndef ZPERIODIC
  endif
#endif    
#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_send_z_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef ZPERIODIC
  endif
#endif  
  ! keep from getting mixed up 
  MSG_XCH_ZLOW_TAG = MSG_XCH_ZLOW_TAG+1
  MSG_XCH_ZHI_TAG = MSG_XCH_ZHI_TAG+1
  return
end subroutine ZExchange
!-----------------------------------------------------------------
subroutine DoXBC(u,p,t,fo)
  use discretization
  use domain
  use mpistuff,only:mype
  implicit none
  type(patcht),intent(in):: p
  type(topot),intent(in)::t
  double precision,intent(in)::fo
  double precision::u(p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  !
  integer:: ii,jj,kk
  double precision,parameter::zero(nvar)=0

#ifndef XPERIODIC
  if (t%ipe%i(1) .eq. 1) then
     ii=0
     do kk=p%all%lo%i(3),p%all%hi%i(3)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           u(ii,jj,kk,:) = bcc(bco,2)*u(ii+2,jj,kk,:) + bcc(bco,1)*u(ii+1,jj,kk,:) !bcc(bco,3)*u(ii+3,jj,kk,:) + 
        enddo
     enddo
  end if
  ! If bdry_type then typeing boundary
  if (t%ipe%i(1) .eq. t%npe%i(1)) then
     ii=p%max%hi%i(1)+1
     do kk=p%all%lo%i(3),p%all%hi%i(3),1
        do jj=p%all%lo%i(2),p%all%hi%i(2),1
           u(ii,jj,kk,:) = bcc(bco,2)*u(ii-2,jj,kk,:) + bcc(bco,1)*u(ii-1,jj,kk,:) ! bcc(bco,3)*u(ii-3,jj,kk,:) + 
        enddo
     enddo
  endif
#endif
  return
end subroutine DoXBC
!-----------------------------------------------------------------
subroutine DoYBC(u,p,t,fo)
  use discretization
  use domain
  use mpistuff,only:mype
  implicit none
  type(patcht),intent(in)::p
  double precision::u(p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  type(topot),intent(in)::t
  double precision,intent(in)::fo
  !
  integer:: ii,jj,kk
  double precision,parameter::zero(nvar)=0
    
  !	yl Boundary: Typeing
#ifndef YPERIODIC	
  if (t%ipe%i(2) .eq. 1) then
     jj=0
     do kk=p%all%lo%i(3),p%all%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1) ! this pet corners but they are wrong!!
           u(ii,jj,kk,:) =  bcc(bco,2)*u(ii,jj+2,kk,:) + bcc(bco,1)*u(ii,jj+1,kk,:)   ! bcc(bco,3)*u(ii,jj+3,kk,:) +      
        enddo
     enddo
  endif
  !	yr Boundary: Typeing
  if (t%ipe%i(2) .eq. t%npe%i(2)) then
     jj=p%max%hi%i(2)+1
     do kk=p%all%lo%i(3),p%all%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           u(ii,jj,kk,:) =   bcc(bco,2)*u(ii,jj-2,kk,:) + bcc(bco,1)*u(ii,jj-1,kk,:) ! bcc(bco,3)*u(ii,jj-3,kk,:) +  
        enddo
     enddo
  endif
#endif  
  return
end subroutine DoYBC
!-----------------------------------------------------------------
subroutine DoZBC(u,p,t,fo)
  use discretization
  use domain
  use mpistuff,only:mype
  implicit none
  type(patcht),intent(in):: p
  double precision::u(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  type(topot),intent(in)::t
  double precision,intent(in)::fo
  !
  integer:: ii,jj,kk
  double precision,parameter::zero=0

#ifndef ZPERIODIC
  if (t%ipe%i(3) .eq. 1) then
     kk=0
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           u(ii,jj,kk,:) =  bcc(bco,2)*u(ii,jj,kk+2,:) + bcc(bco,1)*u(ii,jj,kk+1,:) ! bcc(bco,3)*u(ii,jj,kk+3,:) +
        enddo
     enddo
  endif
  !	zr Boundary: Typeing
  if (t%ipe%i(3) .eq. t%npe%i(3)) then
     kk=p%max%hi%i(3)+1
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           u(ii,jj,kk,:) = bcc(bco,2)*u(ii,jj,kk-2,:) + bcc(bco,1)*u(ii,jj,kk-1,:) ! bcc(bco,3)*u(ii,jj,kk-3,:) + 
        enddo
     enddo
  endif

#endif    
  return
end subroutine DoZBC
!-----------------------------------------------------------------
subroutine ErrorHandler(mpierr,errortype)
  use mpistuff
  implicit none
  integer::mpierr,errortype
  if (mpierr.ne.MPI_SUCCESS) then
     write(0,*) 'SRGMG: MPI RETURN VALUE',mype,mpierr,errortype
  endif
  return
end subroutine ErrorHandler
!-----------------------------------------------------------------
subroutine mysleep(dt)
  !===============================================================================
  implicit none
  integer,dimension(8) :: t ! arguments for date_and_time
  integer :: s1,s2,ms1,ms2  ! start and end times [ms]
  real :: dt                ! desired sleep interval [ms]
  !===============================================================================
  ! Get start time:
  call date_and_time(values=t)
  ms1=(t(5)*3600+t(6)*60+t(7))*1000+t(8)
  
  do ! check time:
     call date_and_time(values=t)
     ms2=(t(5)*3600+t(6)*60+t(7))*1000+t(8)
     if(ms2-ms1>=dt)exit
  enddo
  !===============================================================================
end subroutine mysleep
! normal exchange
subroutine exchange_new(ux,p,t)
  use discretization,only:nvar,nsg
  use base_data_module
  use mpistuff
  use tags
  use globals,only:mg_ref_ratio,redundant_crs
  implicit none
  type(topot),intent(in)::t
  type(patcht),intent(in)::p
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)

  if (.not.redundant_crs.and.t%redundant) then
     if (t%comm3d/=mpi_comm_null) then ! active will inc this - yuck
        MSG_XCH_XLOW_TAG=MSG_XCH_XLOW_TAG + 1
        MSG_XCH_XHI_TAG=MSG_XCH_XHI_TAG + 1
        MSG_XCH_YLOW_TAG=MSG_XCH_YLOW_TAG + 1
        MSG_XCH_YHI_TAG=MSG_XCH_YHI_TAG + 1
        MSG_XCH_ZLOW_TAG=MSG_XCH_ZLOW_TAG + 1
        MSG_XCH_ZHI_TAG=MSG_XCH_ZHI_TAG + 1
     end if
     return
  end if

#ifdef HAVE_PETSC
  call PetscLogEventBegin(events(11),ierr)
#endif
  if (t%comm3d==mpi_comm_null) then
     ! noop
  else
     call Exchange_private(ux,p,t,nsg%i(1))
  end if
#ifdef HAVE_PETSC
  call PetscLogEventEnd(events(11),ierr)
#endif
  return
end subroutine Exchange_new
!-----------------------------------------------------------------------
subroutine Exchange_private(ux,p,t,ng)
  use discretization, only:nvar
  use base_data_module
  use mpistuff
  use tags
  implicit none
  integer,intent(in)::ng
  type(patcht),intent(in)::p
  type(topot),intent(in)::t
  double precision:: ux(&
       p%all%lo%i(1):p%all%hi%i(1),&
       p%all%lo%i(2):p%all%hi%i(2),&
       p%all%lo%i(3):p%all%hi%i(3),nvar)
  ! allocating on stack
  double precision:: xbuffer_lo_send(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  double precision:: xbuffer_lo_recv(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  double precision:: xbuffer_hi_send(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  double precision:: xbuffer_hi_recv(ng,p%max%hi%i(2),p%max%hi%i(3),nvar)
  integer:: XBUFFSIZE
  integer:: ii,jj,mm,kk
  integer:: msg_id_send_x_low
  integer:: msg_id_send_x_hi
  integer:: msg_id_recv_x_low
  integer:: msg_id_recv_x_hi  
  double precision:: ybuffer_lo_send(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  double precision:: ybuffer_lo_recv(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  double precision:: ybuffer_hi_send(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  double precision:: ybuffer_hi_recv(p%all%lo%i(1):p%all%hi%i(1),ng,p%max%hi%i(3),nvar)
  integer:: YBUFFSIZE
  integer:: msg_id_send_y_low
  integer:: msg_id_send_y_hi
  integer:: msg_id_recv_y_low
  integer:: msg_id_recv_y_hi
  double precision:: zbuffer_lo_send(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  double precision:: zbuffer_lo_recv(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  double precision:: zbuffer_hi_send(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  double precision:: zbuffer_hi_recv(p%all%lo%i(1):p%all%hi%i(1),p%all%lo%i(2):p%all%hi%i(2),ng,nvar)
  integer:: ZBUFFSIZE  
  integer:: msg_id_send_z_low
  integer:: msg_id_send_z_hi
  integer:: msg_id_recv_z_low
  integer:: msg_id_recv_z_hi

  XBUFFSIZE = size(xbuffer_lo_send)
  YBUFFSIZE = size(ybuffer_lo_recv)
  ZBUFFSIZE = size(zbuffer_lo_recv)

#ifndef XPERIODIC
  if (t%ipe%i(1).gt.1) then
#endif
     call MPI_Irecv(xbuffer_lo_recv, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%left,MSG_XCH_XLOW_TAG,t%comm3D,msg_id_recv_x_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef XPERIODIC
  endif
#endif
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Irecv(xbuffer_hi_recv, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%right, MSG_XCH_XHI_TAG, t%comm3D,msg_id_recv_x_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef XPERIODIC
  endif
#endif

#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call  MPI_Irecv(ybuffer_lo_recv, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%bottom,MSG_XCH_YLOW_TAG, t%comm3D,msg_id_recv_y_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef YPERIODIC
  endif
#endif  

#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif  
     call MPI_Irecv(ybuffer_hi_recv, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%top, MSG_XCH_YHI_TAG, t%comm3D, msg_id_recv_y_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef YPERIODIC
  endif
#endif  

#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Irecv(zbuffer_lo_recv, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%behind,MSG_XCH_ZLOW_TAG, t%comm3D,msg_id_recv_z_low,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef ZPERIODIC
  endif
#endif

#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif  
     call MPI_Irecv(zbuffer_hi_recv, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%forward, MSG_XCH_ZHI_TAG, t%comm3D, msg_id_recv_z_hi,ierr)
     Call ErrorHandler(ierr,ERROR_RECV)
#ifndef ZPERIODIC
  endif
#endif  

  !       -------X DIRECTION COMMUNICATION
  !       Update x-low boundaries
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              xbuffer_lo_send(mm,jj,kk,:) = ux(p%max%hi%i(1)+1-mm,jj,kk,:)
           enddo
        enddo
     enddo
     if (t%right<0)stop 'p%t%right<0'
     call MPI_Isend(xbuffer_lo_send, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%right,MSG_XCH_XLOW_TAG,t%comm3D,msg_id_send_x_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef XPERIODIC
  endif
#endif
  !	update x-high boundaries
#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              xbuffer_hi_send(mm,jj,kk,:) = ux(mm,jj,kk,:)
           enddo
        enddo
     enddo

     if (t%left<0)stop 't%left<0'
     call MPI_Isend(xbuffer_hi_send, XBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%left, MSG_XCH_XHI_TAG, t%comm3D, msg_id_send_x_hi,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef XPERIODIC
  endif
#endif

#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     call MPI_Wait(msg_id_recv_x_low,status,ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              ux(1-mm,jj,kk,:) = xbuffer_lo_recv(mm,jj,kk,:)
           enddo
        enddo
     enddo
#ifndef XPERIODIC
  endif
#endif 
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Wait(msg_id_recv_x_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do jj=1,p%max%hi%i(2)
           do mm=1,ng
              ux(p%max%hi%i(1)+mm,jj,kk,:) = xbuffer_hi_recv(mm,jj,kk,:)
           enddo
        enddo
     enddo
#ifndef XPERIODIC
  endif
#endif

  ! -------Y DIRECTION COMMUNICATION
  !	update y-low boundaries
#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif  
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do kk=1,p%max%hi%i(3)
           do mm=1,ng
              ybuffer_lo_send(ii,mm,kk,:) = ux(ii,p%max%hi%i(2)+1-mm,kk,:)
           enddo
        enddo
     enddo
     if (t%top<0)stop 't%top<0'
     call MPI_Isend(ybuffer_lo_send, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%top,MSG_XCH_YLOW_TAG,t%comm3D,msg_id_send_y_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef YPERIODIC
  endif
#endif

  !	update y-high boundaries  
#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ybuffer_hi_send(ii,mm,kk,:) = ux(ii,mm,kk,:)
           enddo
        enddo
     enddo
     if (t%bottom<0)stop 't%bottom<0'
     call MPI_Isend(ybuffer_hi_send, YBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%bottom, MSG_XCH_YHI_TAG, t%comm3D, msg_id_send_y_hi,ierr)  
     call errorhandler(ierr,ERROR_SEND)
#ifndef YPERIODIC
  endif
#endif

#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_recv_y_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)     
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ux(ii,1-mm,kk,:) = ybuffer_lo_recv(ii,mm,kk,:)
           enddo
        enddo
     enddo
#ifndef YPERIODIC
  endif
#endif  

#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif
     call MPI_Wait(msg_id_recv_y_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do kk=1,p%max%hi%i(3)
        do ii=p%all%lo%i(1),p%all%hi%i(1)
           do mm=1,ng
              ux(ii,p%max%hi%i(2)+mm,kk,:) = ybuffer_hi_recv(ii,mm,kk,:)
           enddo
        enddo
     enddo
#ifndef YPERIODIC
  endif
#endif    

  ! -------Z DIRECTION COMMUNICATION
  !	update z-low boundaries  
#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif  
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              zbuffer_lo_send(ii,jj,mm,:) = ux(ii,jj,p%max%hi%i(3)+1-mm,:)
           enddo
        enddo
     enddo
     if (t%forward<0)stop 't%forward<0'
     call MPI_Isend(zbuffer_lo_send, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%forward,MSG_XCH_ZLOW_TAG,t%comm3D,msg_id_send_z_low,ierr)
     Call ErrorHandler(ierr,ERROR_SEND)
#ifndef ZPERIODIC
  endif
#endif

  !	update z-high boundaries  
#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              zbuffer_hi_send(ii,jj,mm,:) = ux(ii,jj,mm,:)
           enddo
        enddo
     enddo
     if (t%behind<0)stop 't%behind<0'
     call MPI_Isend(zbuffer_hi_send, ZBUFFSIZE, MPI_DOUBLE_PRECISION,&
          t%behind, MSG_XCH_ZHI_TAG, t%comm3D, msg_id_send_z_hi,ierr)  
     call errorhandler(ierr,ERROR_SEND)
#ifndef ZPERIODIC
  endif
#endif

#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_recv_z_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              ux(ii,jj,1-mm,:) = zbuffer_lo_recv(ii,jj,mm,:)
           enddo
        enddo
     enddo
#ifndef ZPERIODIC
  endif
#endif  

#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif
     call MPI_Wait(msg_id_recv_z_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
     do ii=p%all%lo%i(1),p%all%hi%i(1)
        do jj=p%all%lo%i(2),p%all%hi%i(2)
           do mm=1,ng
              ux(ii,jj,p%max%hi%i(3)+mm,:) = zbuffer_hi_recv(ii,jj,mm,:)
           enddo
        enddo
     enddo
#ifndef ZPERIODIC
  endif
#endif    

! wait on sends
#ifndef XPERIODIC
  if (t%ipe%i(1) .lt. t%npe%i(1)) then
#endif
     call MPI_Wait(msg_id_send_x_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef XPERIODIC
  endif
#endif  
  
#ifndef XPERIODIC
  if (t%ipe%i(1) .gt. 1) then
#endif
     call MPI_Wait(msg_id_send_x_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef XPERIODIC
  endif
#endif
  
#ifndef YPERIODIC
  if (t%ipe%i(2) .lt. t%npe%i(2)) then
#endif
     call MPI_Wait(msg_id_send_y_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef YPERIODIC
  endif
#endif

#ifndef YPERIODIC
  if (t%ipe%i(2) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_send_y_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef YPERIODIC
  endif
#endif  
 
#ifndef ZPERIODIC
  if (t%ipe%i(3) .lt. t%npe%i(3)) then
#endif
     call MPI_Wait(msg_id_send_z_low, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef ZPERIODIC
  endif
#endif

#ifndef ZPERIODIC
  if (t%ipe%i(3) .gt. 1) then
#endif  
     call MPI_Wait(msg_id_send_z_hi, status, ierr)
     Call ErrorHandler(ierr,ERROR_WAIT)
#ifndef ZPERIODIC
  endif
#endif  

  ! keep from getting mixed up 
  MSG_XCH_ZLOW_TAG = MSG_XCH_ZLOW_TAG+1
  MSG_XCH_ZHI_TAG = MSG_XCH_ZHI_TAG+1
  ! keep from getting mixed up 
  MSG_XCH_YLOW_TAG = MSG_XCH_YLOW_TAG+1
  MSG_XCH_YHI_TAG = MSG_XCH_YHI_TAG+1
  ! keep from getting mixed up 
  MSG_XCH_XLOW_TAG = MSG_XCH_XLOW_TAG+1
  MSG_XCH_XHI_TAG = MSG_XCH_XHI_TAG+1

  return
end subroutine Exchange_private
