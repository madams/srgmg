%
% compute utilization of machine with parameters for coarse grid size and shape
%
function max_under = f(maxfact,maxgridsize,startproc,nprocs)
close all
if nargin<4
 nprocs=1700
end
if nargin<3
 startproc=1650
end
if nargin<2
 maxgridsize=128
end
if nargin<1
 maxfact=11
end
m=0;
gap=0;
mgsz=999;
max_under=0;
best_usage=0;
for k2=startproc:nprocs
   k1=k2; numfactors=0;
   while (isinteger(k1/8))
     k1=k1/8;
     numfactors=numfactors+1;
   end
   v=factor(k1);    % what is left over after power of 2 reduction in 3D
   [a b] = size(v); % have 3 factors and largest factor < limit tp prevent too much skew 
   if b > 2 & v(end) <= maxfact,
     qq=v(end-2:end);
     grid_size=prod(qq);
     if grid_size<=maxgridsize,
       m=m+1;
       sizes(m)=grid_size;
       bad_proc=(k2-(gap/2));
       index(m)=bad_proc;
       rel_under=(gap/2)/bad_proc;
       gaps(m)=rel_under;
       if max_under < rel_under, 
         max_under=rel_under; 
         tightest_fit_factors = v
	 tightest_usage = k2/nprocs
       end,
       gap=0;
       if (grid_size<mgsz)
	 mgsz = grid_size;
         smallest_coarse_grid_factors = v
	 smallest_coarse_grid_usage = k2/nprocs
       end;
       if (k2/nprocs>best_usage)
	 best_usage = k2/nprocs
         best_usage_factors = v
         prod(best_usage_factors)
       end;
     else
       gap=gap+1;
     end
  else
     gap=gap+1;
  end,
end,
%index
%semilogx(index,10000*gaps,'rs--'),hold on,
%semilogx(index,sizes,'bd-.'),hold on,
%legend('relative gap to acceptable proc count (x10000)','sizes of coarse grid')
%xlabel('number of processes');
max_under=max_under*100;
prod(best_usage_factors)
