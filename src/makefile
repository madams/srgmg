all:srgmg
#
SHELL=/bin/bash
MOBJ=modules.o
OBJ=kernels_simple.o main.o mg.o bdryexchange.o
#--ar
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

FC_FLAGS += -DHAVE_PETSC -DPARALLELHDF5 -I${HDF5_DIR}/include
HDF5_LIBS = -L${HDF5_DIR}/lib -lhdf5_fortran -lhdf5_hl -lhdf5 -lhdf5hl_fortran
#-diag-disable remark 
#-DTWO_D
srgmg: ${MOBJ} ${OBJ} 
	-${FLINKER} ${FFLAGS} -o pms.${PETSC_ARCH}.ex ${MOBJ} ${OBJ} ${PETSC_SYS_LIB} 
#${HDF5_LIBS} 
#	${RM} ${OBJ} *.mod	

$(OBJ): ${MOBJ}

realclean:
	rm -f *.o *.mod *.history

# regression test -- this is what should work
reg:
	-${MPIEXEC} -n 2 ./pms.${PETSC_ARCH}.ex -plot_hdf5 true -verbose 3 -nxloc 8 -dom_x_hi 2.0 -nxpe 2 -nype 1 -nzpe 1 -smoother_type 1

regsr:
	-${MPIEXEC} -n 2 ./pms.${PETSC_ARCH}.ex -plot_hdf5 true -verbose 4 -nxloc 4 -sr_max_loc_sz 8 -sr_base_bufsz 2 -sr_bufsz_inc 1 -dom_x_hi 2.0 -nxpe 2 -nype 1 -nzpe 1 -smoother_type 1

# regression test (from paper)
regression:
	-${MPIEXEC} -n 16 ./pms.${PETSC_ARCH}.ex -nxloc 4 -verbose 1 -sr_max_loc_sz 64 -sr_base_bufsz 2 -sr_bufsz_inc 1 -dom_x_hi 2.0 -nxpe 4 -nype 2 -nzpe 2 -smoother_type 1 -nsmoothsfmg 1 -nsmoothsup 2 -nsmoothsdown 2 -problem_type 1 -error_norm 3
#      grid i: 1, |f-Au|= 2.120E-02, |u-u~|_3= 7.838E-02 convergence order = 1.875E+00   
#      grid i: 2, |f-Au|= 7.821E-02, |u-u~|_3= 1.883E-02 convergence order = 2.057E+00   
#      grid i: 3, |f-Au|= 2.816E-01, |u-u~|_3= 4.792E-03 convergence order = 1.974E+00 SR
#      grid i: 4, |f-Au|= 2.132E-01, |u-u~|_3= 1.202E-03 convergence order = 1.995E+00 SR
#      grid i: 5, |f-Au|= 1.984E-01, |u-u~|_3= 3.009E-04 convergence order = 1.998E+00 SR
#      grid i: 6, |f-Au|= 7.736E-01, |u-u~|_3= 8.759E-05 convergence order = 1.780E+00 SR

conv:
	-${MPIEXEC} -n 1 ./pms.${PETSC_ARCH}.ex -plot_hdf5 true -verbose 1 -nxloc 64 -smoother_type 1 

valreg:
	-${MPIEXEC} -n 2 valgrind --dsymutil=yes --leak-check=no --gen-suppressions=no --num-callers=20 ./pms.${PETSC_ARCH}.ex -verbose 1 -nxloc 128 -out_error true -show_options true -nsmoothsfmg 0 -nsmoothsup 2 -nsmoothsdown 2 -problem_type 1 -dom_x_hi 2.0 -nxpe 2 -nype 1 -nzpe 1 -smoother_type 1 

NPX=1
NPX2=$(shell echo 2\*$(NPX) | bc)
NP3=$(shell echo $(NPX)*$(NPX)*$(NPX) | bc)
SRLOCN=4
SRFINEST=256
SRBUF_BASE=2
SRBUF_INC=1
NSMDOWN=2
NSMUP=2
NSMFMG=0 # pre V smoothing
VERB=2
OUT_ERROR=true
NP_211=$(shell echo 2*$(NP3) | bc)
SRLOCN2=$(shell echo 2*$(SRLOCN) | bc)
SRFINEST2=$(shell echo 2*$(SRFINEST) | bc)
DOMX=2.0
PTYPE=1
SMOOTHER_TYPE=1
NV=0
BIGB=0
BCORDER=1
SRLOCNHALF=$(shell echo $(SRLOCN)/2 | bc)
SRFINESTHALF=$(shell echo $(SRFINEST)/2 | bc)
NP_411=$(shell echo 4*$(NP3) | bc)
NPX4=$(shell echo 4*$(NPX) | bc)

run:
	-@${MPIEXEC} -n ${NP3} ./pms.${PETSC_ARCH}.ex -nxloc ${SRFINEST} -nyloc ${SRFINEST} -nzloc ${SRFINEST} -nvcycles ${NV} -nfcycles 1 -verbose ${VERB} -show_options true -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -bc_order ${BCORDER}
#-plot_hdf5 true 
#-log_summary

sr:
	-@${MPIEXEC} -n ${NP3} ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCN} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -nvcycles ${NV} -nfcycles 1 -verbose ${VERB} -sr_max_loc_sz ${SRFINEST} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -bc_order ${BCORDER}
#-plot_hdf5 true   

# V only - no SR
runv:
	-@${MPIEXEC} -n ${NP3} ./pms.${PETSC_ARCH}.ex -nxloc ${SRFINEST} -nyloc ${SRFINEST} -nzloc ${SRFINEST} -nvcycles 20 -rtol 1.e-10 -show_options true -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -verbose ${VERB} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -nfcycles 0
#-plot_hdf5 true -nfcycles 0

val:
	-@${MPIEXEC} -n ${NP3} valgrind --dsymutil=yes --suppressions=../minimal.supp --leak-check=no --gen-suppressions=no --num-callers=20 ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCN} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -verbose ${VERB} -sr_max_loc_sz ${SRFINEST} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -out_error ${OUT_ERROR}  -problem_type ${PTYPE}

# sudo /usr/local/bin/gdb
debug:
	-@/usr/bin/gdb --args ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCN} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -verbose ${VERB} -sr_max_loc_sz ${SRFINEST} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -out_error ${OUT_ERROR} 

sr211:
	-@${MPIEXEC} -n ${NP_211} ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCN} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -verbose ${VERB} -sr_max_loc_sz ${SRFINEST} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -nxpe ${NPX2} -nype ${NPX} -nzpe ${NPX} -dom_x_hi ${DOMX} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -redundant_crs true -num_sr_coarse_bufsz ${BIGB} -bc_order ${BCORDER}
#-plot_hdf5 true   

run211:
	-@${MPIEXEC} -n ${NP_211} ./pms.${PETSC_ARCH}.ex -nxloc ${SRFINEST} -nyloc ${SRFINEST} -nzloc ${SRFINEST} -verbose ${VERB} -show_options true -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -nxpe ${NPX2} -nype ${NPX} -nzpe ${NPX} -dom_x_hi ${DOMX} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -bc_order ${BCORDER} -num_solves 8
#-plot_hdf5 true 

sr411:
	-@${MPIEXEC} -n ${NP_411} ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCNHALF} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -verbose ${VERB} -sr_max_loc_sz ${SRFINESTHALF} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -nxpe ${NPX4} -nype ${NPX} -nzpe ${NPX} -dom_x_hi ${DOMX} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -redundant_crs true -num_sr_coarse_bufsz ${BIGB} -bc_order ${BCORDER} -plot_hdf5 true   


runbigb:
	-@${MPIEXEC} -n ${NP3} ./pms.${PETSC_ARCH}.ex -nxloc ${SRLOCN} -nyloc ${SRLOCN} -nzloc ${SRLOCN} -nvcycles ${NV} -nfcycles 1 -verbose ${VERB} -sr_max_loc_sz ${SRFINEST} -show_options true -sr_base_bufsz ${SRBUF_BASE} -sr_bufsz_inc ${SRBUF_INC} -nsmoothsup ${NSMUP} -nsmoothsdown ${NSMDOWN} -out_error ${OUT_ERROR} -problem_type ${PTYPE} -nsmoothsfmg ${NSMFMG} -smoother_type ${SMOOTHER_TYPE} -num_sr_coarse_bufsz ${BIGB} -bc_order ${BCORDER}
